#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <string.h>
#include <stdbool.h>
#include <errno.h>
#include <unistd.h>
#include <stdint.h>

#define BUFFER_SIZE 1024

const char* path = "/tmp/yinput_cmder.socket";

void handleError(char *msg) { //错误处理函数
    perror(msg);
    exit(-1);
}

int main(int argc, char* argv[]) {
    if (argc <= 0) {
        return 130;
    }

    char* buffer = argv[1];

    struct sockaddr_un addr;
    addr.sun_family = AF_UNIX;
    strncpy(addr.sun_path, path, sizeof(addr.sun_path));


    int clientSocket = socket(AF_UNIX, SOCK_SEQPACKET, 0);
    if (clientSocket == -1) {
        handleError("创建socket失败");
    }

    if (connect(clientSocket, (struct sockaddr *)&addr, sizeof(addr)) == -1) {
        handleError("连接服务端失败");
    }

    printf("write data: %ld\n", strlen(buffer));
    if(send(clientSocket, buffer, strlen(buffer), 0)==-1) {
        handleError("发送失败");
    }
    close(clientSocket);

    return 0;
}
