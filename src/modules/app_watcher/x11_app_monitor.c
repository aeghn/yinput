#include <X11/Xlib.h>
#include <stdlib.h>
#include <string.h>

#include "constant.h"
#include "utils/logger.h"
#include "utils/singleton.h"

#define MAXSTR 1000

Window last_window = -1;
unsigned char* last_window_class = (unsigned char*) "";

// https://github.com/UltimateHackingKeyboard/current-window-linux/blob/master/get-current-window.c
bool check_status(int status, unsigned long window) {
    if (status == BadWindow) {
        log_error("window id # 0x%lx does not exists!", window);
        return FALSE;
    }

    if (status != Success) {
        log_error("XGetWindowProperty failed!");
        return FALSE;
    }

    return TRUE;
}


unsigned char* get_string_property(char* property_name, Display* display, Window window) {
    Atom actual_type, filter_atom;
    int actual_format, status;
    unsigned long nitems, bytes_after;
    unsigned char* prop;

    filter_atom = XInternAtom(display, property_name, True);
    status = XGetWindowProperty(display, window, filter_atom, 0, MAXSTR, False, AnyPropertyType,
        &actual_type, &actual_format, &nitems, &bytes_after, &prop);
    if (check_status(status, window)) {
        return prop;
    } else {
        return NULL;
    }
}

unsigned long get_long_property(char* property_name, Display* display, Window window) {
    unsigned char* prop = get_string_property(property_name, display, window);
    if (prop == NULL) {
        return 0;
    }
    
    unsigned long long_property = prop[0] + (prop[1]<<8) + (prop[2]<<16) + (prop[3]<<24);
    return long_property;
}

int main() {
    log_info("yiapp-watcher is starting");

    if (is_singleton_existed("yiapp-watcher")) {
        exit(1);
    }

    /*
     * Open display,
     */
    const char* display_name = NULL;
    Display* display = XOpenDisplay(display_name);

    // If XOpenDisplay does not succeed, it returns NULL.
    if (!display) {
        log_error("Unable to open display: %s",  XDisplayName(display_name));
        exit(1);
    }

    // The DefaultRootWindow macro returns the root window for the default screen.
    Window dwindow = DefaultRootWindow(display);


    XSetWindowAttributes attributes = { .event_mask = PropertyChangeMask };

    XChangeWindowAttributes(display, dwindow, CWEventMask, &attributes);


    while (TRUE) {
        XEvent event;
        XNextEvent(display, &event);

        Window window = get_long_property("_NET_ACTIVE_WINDOW", display, dwindow);
        
        if (window == last_window) {
            log_debug("same window `%d'", window);
            continue;
        } else {
            last_window = window;
        }

        unsigned char* window_class = NULL;
        if (window == 0) {
            window_class = (unsigned char*) DEFAULT_WINDOW_CLASS;   
        } else {
            window_class = get_string_property("WM_CLASS", display, window);
        }

        if (strcmp((const char*) window_class, (const char*) last_window_class) == 0) {
            log_debug("same window class `%s'", window_class);
            continue;
        }

        log_debug("window class `%s'", window_class);
    }


    XCloseDisplay(display);
    return 0;
}
