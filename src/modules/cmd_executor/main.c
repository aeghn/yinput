#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/wait.h>
#include <time.h>
#include <unistd.h>


#include "constant.h"
#include "utils/logger.h"
#include "utils/singleton.h"
#include "utils/uds.h"

void run_command(const char *command) {
    if (strlen(command) == 0) {
        log_warn("command is empty");
        return;
    }

    log_info("command: %s", command);
    pid_t pid;

    //  if (fork() == 0)
    //  execlp ("sh", "sh", "-c", key->command, NULL);
    if (!(pid = fork())) {
        setsid();

        switch (fork()) {
        case 0:
            execlp("sh", "sh", "-c", command, (char *) NULL);
            break;
        default:
            _exit(0);
            break;
        }
    }
    if (pid > 0)
        wait(NULL);
}

int main() {
    log_info("start command executor.");

    if (is_singleton_existed("yicmder")) {
        exit(1);
    }

    log_info("begin to create socket.");

    char payload[2000];
    int sock_fd = create_server_socket(CMDER_SOCKET);

    while (TRUE) {
        int con;
        struct sockaddr_un sock_addr;
        socklen_t sock_addr_len = sizeof(sock_addr);
        getsockname(sock_fd, (struct sockaddr *) &sock_addr, &sock_addr_len);
        log_info("try to accept socket: %s", sock_addr.sun_path);

        if ((con = accept(sock_fd, NULL, NULL)) < 0) {
            log_error("accept: %s", strerror(errno));
            sleep(1);
            continue;
        }

        for (;;) {
            size_t sz = read(con, &payload, sizeof(payload));

            log_debug("received: %s", payload);
            
            if (sz < 0) {
                log_error("read content: %s", strerror(errno));
                exit(-1);
            }

            payload[sz] = '\0';

            if (strlen(payload) == 0) {
                int size = getpeername(sock_fd, NULL, NULL);
                if (size == -1) {
                    if (errno == ENOTCONN) {
                        log_error("unable to get peername: %s", strerror(errno));
                        break;
                    }
                }
                log_error("error %s", strerror(errno));
            }


            run_command(strdup(payload));
        }
    }
}
