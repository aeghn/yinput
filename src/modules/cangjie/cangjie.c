#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <ctype.h>

#include "constant.h"
#include "cangjie.h"
#include "utils/logger.h"

typedef struct _cangjie_node {
    char* chars;

    struct _cangjie_node *nodes[25];
} cangjie_node;

cangjie_node *root_node;

static cangjie_node* create_node() {
    cangjie_node* pnode = malloc(sizeof(cangjie_node));

    pnode->chars = NULL;
    for (int i = 0; i < 25; i++) {
        pnode->nodes[i] = NULL;
    }

    return pnode;
}


static void insert_node(const char* cangjie, const char *hanzi) {
    cangjie_node* pnode = root_node;

    for (const char* c = cangjie; c; c++) {
        int i = (*c) - 'A';
        if (i < 0) {
            break;
        }

        if (pnode->nodes[i] == NULL) {
            pnode->nodes[i] = create_node();
        }

        pnode = pnode->nodes[i];
    }

    if (pnode == root_node) {
        log_error("empty node: %s", cangjie);
        exit(1);
    }

    char* p = NULL;
    if (pnode->chars == NULL) {
        pnode->chars = malloc(strlen(hanzi)+1);
        p = pnode->chars; 
    } else {
        int ol = strlen(pnode->chars);
        pnode->chars = realloc(pnode->chars, ol + strlen(hanzi));
        p = pnode->chars + ol;
    }

    snprintf(p, strlen(hanzi) + 1, "%s", hanzi);
    *(p + strlen(hanzi) + 1) = 0;
}

static void parse_file(const char* file_path) {
    FILE* file = fopen(file_path, "r");

    char* hanzi = malloc(10);
    char* cangjie = malloc(10);
    bool hanzi_is_set = FALSE;
    char *ph = hanzi, *pc = cangjie;

    char ch;
    while((ch = getc(file)) != EOF) {
        if (ch == '\n') {
            *ph = 0;
            *pc = 0;

            insert_node(cangjie, hanzi);
            ph = hanzi;
            pc = cangjie;
            hanzi_is_set = FALSE;
            continue;
        }

        if (ch == ' ' || ch == '\t') {
            if (!hanzi_is_set && ph - hanzi > 0) {
                hanzi_is_set = TRUE;
            }
            continue;
        }

        if (!hanzi_is_set) {
            *(ph++) = ch;
        } else {
            *(pc++) = ch;
        }

    }

    fclose(file);
    free(hanzi);
    free(cangjie);
}

void cangjie_build_table() {
    log_info("begin to build cangjie table");

    root_node = create_node();

    char* path = "/home/chin/Repos/yinput/src/modules/cangjie/cangjie.table";
    parse_file(path);

    log_info("finished building cangjie table");
}

static void traversal(cangjie_node *enter, char** p, char* str) {
    // log_info("%s, %s  ", p, str);

    if (enter == NULL || *p - str > 500) {
        return;
    }

    if (enter->chars != NULL) {
        char* pc = enter->chars;
        while (*pc) {
            *((*p)++) = *(pc++);
        }
    }

    for (int i =0; i < 25; i++) {
        traversal(enter->nodes[i], p, str);
    }
}

char* cangjie_start_with(char* s) {
    log_info("begin to get char %s", s);

    cangjie_node *pn = root_node;
    for (char* p = s; *p; p++) {
        int i;
        if (*p >= 'a' && *p <= 'z') {
            i = *p - 'a';
        } else if (*p >= 'A' && *p <= 'Z') {
            i = *p - 'A';
        } else {
            log_debug("other condition");
            break;
        }

        pn = pn->nodes[i];
        if (pn == NULL) {
            log_debug("pn is null");
            return NULL;
        }
    }

    char* str = malloc(505 * sizeof(char));
    char* p = str;

    traversal(pn, &p, str);
    *p = 0;
    log_info("string: %s", str);

    return NULL;
}
