#ifndef UDS_SERVER
#define UDS_SERVER

int create_server_socket(const char* socket_file);

const char* server_read(int sfd);

#endif
