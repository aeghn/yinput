#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include "constant.h"
#include "logger.h"
#include "singleton.h"


bool is_singleton_existed(const char* process_name) {
    char lock_path[108];
    sprintf(lock_path, "/tmp/%s.lock", process_name);

    int fd = open(lock_path, O_RDWR | O_CREAT, 0666);
    if (fd < 0) {
        log_error("Open file failed, error: %s", strerror(errno));
        exit(1);
    }

    struct flock fl;
    fl.l_type = F_WRLCK;
    fl.l_start = 0;
    fl.l_whence = SEEK_SET;
    fl.l_len = 0;
    int ret = fcntl(fd, F_SETLK, &fl);
    if (ret < 0) {
        if (errno == EACCES || errno == EAGAIN) {
            log_error("`%s' was already locked, error: %s\n", lock_path, strerror(errno));
            close(fd);
        }
        return TRUE;
    }

    char buf[16] = {'\0'};
    sprintf(buf, "%d", getpid());
    ftruncate(fd, 0);
    ret = write(fd, buf, strlen(buf));
    if (ret < 0) {
       log_error("Write file failed, file: %s, error: %s\n", lock_path, strerror(errno));
       close(fd);
       exit(1);
    }

    return FALSE;
}
