#include <time.h>

unsigned long get_time_ms() {
    struct timespec tv;
    clock_gettime(CLOCK_MONOTONIC, &tv);
    return (tv.tv_sec*1E3) + tv.tv_nsec/1E6;
}
