#include <time.h>
#include <stdio.h>
#include "logger.h"

struct _log_type {
    char* type;
    char* color;
    char channel;
};

static struct _log_type _debug = {"DEBUG", "\x1b[36m", 1};
static struct _log_type _info = {"INFO", "\x1b[32m", 1};
static struct _log_type _warn = {"WARN", "\x1b[33m", 2};
static struct _log_type _error = {"ERROR", "\x1b[31m", 2};

void log_log(int log_type, const char* file, int line, const char* fmt, ...) {
    struct _log_type* type;
    if (log_type == LOG_INFO) {
        type = &_info;
    } else if (log_type == LOG_WARN) {
        type = &_warn;
    } else if (log_type == LOG_ERROR) {
        type = &_error;
    } else if (log_type == LOG_DEBUG) { 
        type = &_debug;
    } else {
        type = &_info;
    }

    void* chan = type->channel == 1 ? stdout : stderr;

    char human_time[20];

    time_t t = time(NULL);
    struct tm *time;
    time = localtime(&t);
    human_time[strftime(human_time, sizeof(human_time), "%Y-%m-%d %H:%M:%S", time)] = '\0';

    va_list ap;
    va_start(ap, fmt);
    fprintf(chan, "%s * %-5s %s %s:%d\x1b[0m: ", type->color, type->type, human_time, file, line);
    vfprintf(chan, fmt, ap);
    fprintf(chan, "\n");
    fflush(chan);
    va_end(ap);
}
