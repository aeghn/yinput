#include <errno.h>
#include <fcntl.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/un.h>
#include <unistd.h>

#include "logger.h"
#include "uds.h"

#define MAX_MSG_LENGTH 65535

int create_server_socket(const char* socket_file) {
    int sd = socket(AF_UNIX, SOCK_SEQPACKET, 0);
    struct sockaddr_un addr = {0};

    if (sd < 0) {
        perror("socket");
        exit(-1);
    }
    addr.sun_family = AF_UNIX;
    strncpy(addr.sun_path, socket_file, sizeof(addr.sun_path)-1);

    unlink(socket_file);
    if (bind(sd, (struct sockaddr *) &addr, sizeof addr) < 0) {
        log_error("unable to bind `%s', %s", socket_file, strerror(errno));
        exit(-1);
    }

    if (listen(sd, 20) < 0) {
        perror("listen");
        exit(-1);
    }

    chmod(socket_file, 0660);
    fcntl(sd, F_SETFL, O_SYNC);

    log_info("create socket successfully: %s", socket_file);
    return sd;
}

const char* server_read(int sfd) {
    char payload[MAX_MSG_LENGTH];
    int con, sz = 0;

    if ((con = accept(sfd, NULL, NULL)) < 0) {
        log_error("accept: %s", strerror(errno));
        exit(1);
    }

    sz = read(con, &payload, sizeof(payload));
    if (sz < 0) {
        log_error("read content: %s", strerror(errno));
        exit(-1);
    }
    payload[sz] = '\0';

    close(con);

    return strdup(payload);
}
