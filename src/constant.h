#ifndef CONSTANT_H
#define CONSTANT_H

typedef int bool;

#define TRUE 1
#define FALSE 0

#define CMDER_SOCKET "/tmp/yinput_cmder.socket"

#define DEFAULT_WINDOW_CLASS "__DefWM"
#define DEFAULT_WINDOW_ID -404

#define MAX_KEYBOARD 256

#endif
