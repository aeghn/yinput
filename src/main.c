#define MAX_KEYBOARDS 32

#ifdef __FreeBSD__
#include <dev/evdev/input.h>
#include <dev/evdev/uinput.h>
#else
#include <linux/input.h>
#include <linux/uinput.h>
#endif

#include <stdio.h>
#include <sys/time.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/file.h>
#include <dirent.h>
#include <assert.h>
#include <errno.h>
#include <string.h>
#include <sys/stat.h>
#include <signal.h>
#include <unistd.h>
#include <termios.h>
#include <stdint.h>
#include <stdarg.h>
#include <time.h>
#include <stdlib.h>
#include <fcntl.h>
#include <grp.h>
#include <sys/inotify.h>

#include "evdev.h"
#include "constant.h"
#include "utils/logger.h"
#include "modules/cangjie/cangjie.h"

struct ykbd {
    int fd;
    char node[1024];
    char name[1024];

    struct ykbd *next;
};


struct ykbd *kbds = NULL;

struct ykbd* build_kbd(const char* devnode) {
    int fd;
    const char *name;
    uint32_t id;
    uint16_t  vendor_id, product_id;

    name = evdev_device_name(devnode);
    if (!strstr(name, "RAPOO")) {
        return NULL;
    }

    id = evdev_device_id(devnode);
    vendor_id = id >> 16;
    product_id = id & 0xFFFF;

    if ((fd = open(devnode, O_RDONLY | O_NONBLOCK)) < 0) {
        log_error("enable to open %s, %s",  devnode, strerror(errno));
        exit(1);
    }

    // Grab the keyboard.
    if (ioctl(fd, EVIOCGRAB, (void *) 1) < 0) {
        log_info("Failed to grab %04x:%04x, ignoring...\n", vendor_id, product_id);
        perror("EVIOCGRAB");
        close(fd);
        return NULL;
    }

    struct ykbd *kbd = malloc(sizeof(struct ykbd));
    kbd->fd = fd;
    strcpy(kbd->node, devnode);
    snprintf(kbd->name, strlen(name)+1, "%s", name);

    return kbd;
}

void build_kbds() {
    char *devnodes[1024];
    int size;

    evdev_get_keyboard_nodes(devnodes, &size);
    for (int i = 0; i < size; i++) {
        struct ykbd *kbd = build_kbd(devnodes[i]);
        if (kbd == NULL) {
            continue;
        }

        kbd->next = kbds;
        kbds = kbd;
    }
}

void main_loop() {
    build_kbds();

    for (struct ykbd *kbd = kbds; kbd; kbd = kbd->next) {
        log_info("kbd: %d; %s; %s", kbd->fd, kbd->node, kbd->name);

        int fd = kbd->fd;

        struct input_event ev;

        int state;

        while (TRUE) {
            int maxfd = 0;
            fd_set fds;

            FD_ZERO(&fds);
            for (kbd = kbds; kbd; kbd = kbd->next) {
                int fd = kbd->fd;

                maxfd = maxfd > fd ? maxfd : fd;
                FD_SET(fd, &fds);
            }

            if (select(maxfd + 1, &fds, NULL, NULL, NULL) < 0) {
                continue;
            }

            while ((state = read(fd, &ev, sizeof(ev))) > 0) {

                
                switch (ev.type) {
                case EV_KEY:
                    log_info("%ld, %d, %d, %d", ev.time.tv_usec, ev.code, ev.type, ev.value);
                    break;
                case EV_REL: /* Pointer motion events */
                case EV_MSC:
                case EV_SYN:
                    break;
                default:
                    log_warn("Unrecognized event: (type: %d, code: %d, value: %d)", ev.type, ev.code, ev.value);
                }
            }

            // log_debug("%d, %s", state, strerror(errno));
        }
    }
}

void build_cangjie() {
    cangjie_build_table();
}

int main() {
    // main_loop();
    build_cangjie();

    char s[2] = {'A', 0};
    cangjie_start_with(s);
    cangjie_start_with("B");
    cangjie_start_with("HQI");
    
}
