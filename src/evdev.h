#include <stdint.h>
#include <stdio.h>

// static struct keyboard *keyboards = NULL;

int evdev_is_keyboard(const char *devnode);
void evdev_get_keyboard_nodes(char **devs, int *ndevs);
const char *evdev_device_name(const char *devnode);
uint32_t evdev_device_id(const char *devnode);
